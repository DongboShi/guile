# smart_rbind函数 用于rbind两个可能存在不同列的表，取最小公倍数的列作为合并后的列
# 如果有一个输入为空，则返回另一个表

smart_rbind <- function(data1,data2){
        if(dim(data1)[1]==0){
                return(data2)
        }else if(dim(data2)[1]==0){
                return(data1)
        }else{
                colm <- unique(c(names(data1),names(data2)))
                if(length(colm[!colm%in%names(data1)])>0){
                        for(col in colm[!colm%in%names(data1)]){
                                data1$tmp <- NA
                                names(data1)[length(names(data1))]<-col
                        }
                }
                if(length(colm[!colm%in%names(data2)])>0){
                        for(col in colm[!colm%in%names(data2)]){
                                data2$tmp <- NA
                                names(data2)[length(names(data2))]<-col
                        }
                }
                data1 <- data1[colm]
                data2 <- data2[colm]
                data <- rbind(data1,data2)
                return(data)
        }
        
}